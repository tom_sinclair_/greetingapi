namespace GreetingAPI
{
    public class Greeting
    {
        public int Id { get; set; }
        public string Salute { get; set; }
        public string Locale { get; set; }
        public Greeting()
        {
        }
        }
 
    }
