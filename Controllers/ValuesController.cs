using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace GreetingAPI.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        Greeting[] greetings = new Greeting[] 
        {
            new Greeting { Id = 1, Salute = "Hello, World!", Locale = "en-us"},
            new Greeting { Id = 1, Salute = "Bonjour tout le monde", Locale = "fr-fr"}
        };
        // GET api/values
        [HttpGet]
        public IEnumerable<Greeting> Get()
        {
            return greetings;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var greeting = greetings.FirstOrDefault((p) => p.Id == id);
            if (greeting == null)
            {
                return NotFound();
            }
            return Ok(greeting);

        }
    }
}
